//nsolic //nolinea
'use strict';
module.exports = function(SolicCa){

  SolicCa.deleteByNoLinea = deleteByNoLinea;

  SolicCa.remoteMethod('deleteByNoLinea', {
    accepts: [{
      arg: 'nsolic',
      type: 'number',
    },
    {
      arg: 'nolinea',
      type: 'number',
    },
  ],
    returns: {
      arg: 'info',
      type: 'object',
    },
    http: {
      path: '/deleteByNoLinea',
      verb: 'delete',
    },
  });

  function deleteByNoLinea(nsolic, nolinea, cb) {
    console.log(nsolic + ' ' + nolinea);
    if (nsolic  && nolinea) SolicCa.destroyAll({nsolic: nsolic, nolinea: nolinea})
        .then(function(info) {
          cb(null, info);
        })
        .catch(function(err) {
          cb(err, null);
        });
    else cb(new Error('Not Found arguments'), null);
  };
};
