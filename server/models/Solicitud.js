'use strict';

module.exports = function(Solicitud) {
    var app = require("../../server/server");
    var REMOTE_METHOD_DESC =
        "Obtiene todas las cotizaciones: " +
        "tarjeta plastica, etiquetas, impresos comerciales";
    var REMOTE_METHOD_DESC_2 =
        "Obtiene todas las cotizaciones: " +
        "tarjeta plastica, etiquetas, impresos comerciales";
    var REMOTE_METHOD_DESC_3 =
        "Obtiene una cotizacion en base a su ncot: ";
    Solicitud.allCotiz = allCotiz;
    Solicitud.allCotizObj = allCotizObj;
    Solicitud.allCotizById = allCotizById;

    Solicitud.remoteMethod("allCotiz", {
        description: REMOTE_METHOD_DESC,
        returns: { arg: "data", type: "Array" },
        http: { path: "/allCotiz", verb: "get" }
    });

    Solicitud.remoteMethod("allCotizObj", {
        description: REMOTE_METHOD_DESC_2,
        returns: { arg: "data", type: "Array" },
        http: { path: "/allCotizObj", verb: "get" }
    });

    Solicitud.remoteMethod("allCotizById", {
        description: REMOTE_METHOD_DESC_3,
        accepts: [{
            arg: 'numsol',
            type: 'number',
        }
        ],
        returns: { arg: "data", type: "object" },
        http: { path: "/allCotizById", verb: "get" }
    });

    /**
       * funcion que retorna todas las cotizaciones:
       * Tarjeta plastica, Etiquetas, Impresos Comerciales
       * @var cb {function} callback
       */
    function allCotiz(cb) {
        var results = [];
        var SolicCotiztp = app.models.SolicCotiztp;
        var SolicCotizic = app.models.SolicCotizic;
        var SolicCotizet = app.models.SolicCotizet;

        SolicCotiztp.find({}, function (err, dataTp) {
            if (err) cb(err);
            for (var i = 0; i < dataTp.length; i++) results.push(dataTp[i].numsol);
            SolicCotizic.find({}, function (err, dataIp) {
                if (err) cb(err);
                for (var i = 0; i < dataIp.length; i++) results.push(dataIp[i].numsol);
                SolicCotizet.find({}, function (err, dataEt) {
                    if (err) cb(err);
                    for (var i = 0; i < dataEt.length; i++)
                        results.push(dataEt[i].numsol);
                    results.sort(function (a, b) {
                        return b - a;
                    });
                    cb(null, results);
                });
            });
        });
    }

    /**
       * funcion que retorna todas las cotizaciones:
       * Tarjeta plastica, Etiquetas, Impresos Comerciales
       * @var cb {function} callback
       */
    function allCotizObj(cb) {
        var results = [];
        var SolicCotiztp = app.models.SolicCotiztp;
        var SolicCotizic = app.models.SolicCotizic;
        var SolicCotizet = app.models.SolicCotizet;

        SolicCotiztp.find({}, function (err, dataTp) {
            if (err) cb(err);
            for (var i = 0; i < dataTp.length; i++) results.push(dataTp[i]);
            SolicCotizic.find({}, function (err, dataIp) {
                if (err) cb(err);
                for (var i = 0; i < dataIp.length; i++) results.push(dataIp[i]);
                SolicCotizet.find({}, function (err, dataEt) {
                    if (err) cb(err);
                    for (var i = 0; i < dataEt.length; i++)
                        results.push(dataEt[i]);
                    results.sort(function (a, b) {
                        return b - a;
                    });
                    cb(null, results);
                });
            });
        });
    }

    /**
       * funcion que retorna todas las cotizaciones:
       * Tarjeta plastica, Etiquetas, Impresos Comerciales
       * @var cb {function} callback
       */
    function allCotizById(numsol, cb) {
        var result = {}; 
        var SolicCotiztp = app.models.SolicCotiztp;
        var SolicCotizic = app.models.SolicCotizic;
        var SolicCotizet = app.models.SolicCotizet;

        SolicCotiztp.find({ where: { numsol: numsol } }, function (err, dataTp) {
            if (err) cb(err);
            if (dataTp[0]) {
                dataTp[0].tipo = 'tp';
                result = dataTp[0];
            }

            SolicCotizic.find({ where: { numsol: numsol } }, function (err, dataIp) {
                if (err) cb(err);
                if (dataIp[0]) {
                    dataIp[0].tipo = 'ic';
                    result = dataIp[0];
                }

                SolicCotizet.find({ where: { numsol: numsol } }, function (err, dataEt) {
                    if (err) cb(err);
                    if (dataEt[0]) {
                        dataEt[0].tipo = 'et';
                        result = dataEt[0];
                    }

                    cb(null, result);
                });
            });
        });
    }
}