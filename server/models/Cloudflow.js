'use strict';

module.exports = function (Cloudflow) {
    var cloudflowAPI = require('cloudflow-api');
    var apiAsync = cloudflowAPI.getAsyncAPI('http://10.1.10.5:9090');
    Cloudflow.getContact = getContact;
    //Contacto.getContact = getContact;
    
    Cloudflow.remoteMethod('getContact', {
            accepts: [{
                arg: 'email',
                type: 'string'
            }],
            http: {
                verb: 'post', path: '/obtenerContactoPorEmail'
            },
            returns: {
                arg: 'data',
                type: 'object',
                root: true
            },
        });

        function getContact(email, cb) {
            if (email) {
                getCFContactByEmail(email)
                    .then((data) => {
                        cb(null, data);
                    })
                    .catch((err) => {
                        cb(err, null);
                    })
            }
        }
    
        function getCFContactByEmail(email) {
            return new Promise((resolve, reject) => {
                apiAsync.auth.create_session('admin',
                    'admin',
                    function (session) {
                        apiAsync.m_session = session.session;
                        apiAsync.users.get_by_email(email, function (data) {
                            resolve(data);
                        }, function (err) {
                            reject(err);
                        })
                    },
                    function (err) {
                        reject(err);
                    });
            });
        }
        



}