//nsolic,nolinea,secuencia,codigo
'use strict';
module.exports = function(SolicMotiv) {

    var REMOTE_METHOD_DESC = 'Elimino los motivos de la parte en base a nsolic, consecSolic y nolinea'

    //SolicMotiv.modifyTabCosByCodigo = modifyTabCosByCodigo;
    SolicMotiv.delByPk = delByPk;

    // SolicMotiv.remoteMethod('modifyTabCosByCodigo', {
    //     accepts: [{
    //         arg: 'objtabcos',
    //         type: 'object',
    //     }],
    //     returns: {
    //         arg: 'info',
    //         type: 'object',
    //     },
    //     http: {
    //         path: '/modifyTabCosByCodigo',
    //         verb: 'post',
    //     },
    // });

    SolicMotiv.remoteMethod("delByPk", {
        accepts: [
            {
              arg: 'nsolic',
              type: 'number',
            },
            {
              arg: 'consecSolic',
              type: 'String',
            },
            {
              arg: 'nolinea',
              type: 'number',
            }
        ],
        description: REMOTE_METHOD_DESC,
        returns: { 
            arg: "info", 
            type: "object" 
        },
        http: { 
            path: "/delByPk",
            verb: "delete" 
        }
    });

  function delByPk(nsolic, consecSolic, nolinea, cb) {
    console.log("Eliminando motivo nsolic: " + nsolic + ' consecSolic: ' + consecSolic + " nolinea: " + nolinea);
    if (nsolic  && consecSolic && nolinea){
        SolicMotiv.destroyAll({nsolic: nsolic, consecSolic: consecSolic, nolinea: nolinea})
        .then(function(info) {
          cb(null, info);
        })
        .catch(function(err) {
          cb(err, null);
        });
    }else{ 
        cb(new Error('Not Found arguments'), null);
    }
  };

    // function modifyTabCosByCodigo(objtabcos, cb) {
    //     if (objtabcos)
    //         Tabcos.updateAll({ and: [{ nsolic: objtabcos.nsolic }, { nolinea: objtabcos.nolinea }, { secuencia: objtabcos.secuencia }, { codigo: objtabcos.codigo }] }, objtabcos, function(err, data) {
    //             if (err) {
    //                 cb(err, null);
    //             }
    //             cb(null, data);
    //         })

    //     else cb(new Error('Not Found arguments'), null);
    // };
};