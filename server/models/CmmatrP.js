'use strict';

module.exports = function (CmmatrP) {
    CmmatrP.afterRemote('**', cmmatrPLoaded);

    /**
     * Hook when find is invoked (findAll, findOne.. etc)
     * @param {*} ctx 
     * @param {*} next 
     */
    function cmmatrPLoaded(ctx, cmmatr, next) {
        //if It's an array we'll filter duplicated data
        if(Array.isArray(cmmatr)) {
            let arr = cmmatr;
            arr = arr.filter((thing, index, self) =>  
            self.findIndex(t => t.codmat === thing.codmat) === index);
            ctx.result = arr;
            next();
        } else {
            next();
        }
    }
}
