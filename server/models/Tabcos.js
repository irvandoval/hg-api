//nsolic,nolinea,secuencia,codigo
'use strict';
module.exports = function(Tabcos) {

    var REMOTE_METHOD_DESC = 'Elimino el costo de la parte en base al numero de solicitud y el consec'

    Tabcos.modifyTabCosByCodigo = modifyTabCosByCodigo;
    Tabcos.delByNSolicYConsec = delByNSolicYConsec;

    Tabcos.remoteMethod('modifyTabCosByCodigo', {
        accepts: [{
            arg: 'objtabcos',
            type: 'object',
        }],
        returns: {
            arg: 'info',
            type: 'object',
        },
        http: {
            path: '/modifyTabCosByCodigo',
            verb: 'post',
        },
    });

    Tabcos.remoteMethod("delByNSolicYConsec", {
        accepts: [
            {
              arg: 'nsolic',
              type: 'number',
            },
            {
              arg: 'consec',
              type: 'String',
            },
            {
              arg: 'nolinea',
              type: 'number',
            },
            {
              arg: 'secuencia',
              type: 'String',
            },
            {
              arg: 'codigo',
              type: 'String',
            },
        ],
        description: REMOTE_METHOD_DESC,
        returns: { 
            arg: "info", 
            type: "object" 
        },
        http: { 
            path: "/delByNSolicYConsec",
            verb: "delete" 
        }
    });

  function delByNSolicYConsec(nsolic, consec, nolinea, secuencia, codigo, cb) {
    console.log("Eliminando solitiud n: " + nsolic + ' consec: ' + consec + " nolinea: " + nolinea + " secuencia: " + secuencia + " codigo: " + codigo);
    if (nsolic  && consec && nolinea && secuencia && codigo){
        Tabcos.destroyAll({nsolic: nsolic, consec: consec, nolinea: nolinea, secuencia: secuencia, codigo: codigo})
        .then(function(info) {
          cb(null, info);
        })
        .catch(function(err) {
          cb(err, null);
        });
    }else{ 
        cb(new Error('Not Found arguments'), null);
    }
  };

    function modifyTabCosByCodigo(objtabcos, cb) {
        if (objtabcos)
            Tabcos.updateAll({ and: [{ nsolic: objtabcos.nsolic }, { nolinea: objtabcos.nolinea }, { secuencia: objtabcos.secuencia }, { codigo: objtabcos.codigo }] }, objtabcos, function(err, data) {
                if (err) {
                    cb(err, null);
                }
                cb(null, data);
            })

        else cb(new Error('Not Found arguments'), null);
    };
};