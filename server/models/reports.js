'use strict';

module.exports = function(Reports){

	const client = require("jsreport-client")("http://localhost:5488", "admin", "password");
	var fs = require('fs');

	Reports.crearReporteET = crearReporteET;

	  Reports.remoteMethod('crearReporteET', {
	    accepts: [
	    { arg: 'dia', type: 'string' },
	    { arg: 'mes', type: 'string' },
	    { arg: 'anio', type: 'string' },
	    { arg: 'ejecutivo', type: 'string' },
	    { arg: 'empresa', type: 'string' },
	    { arg: 'nit', type: 'string' },
	    { arg: 'cargo', type: 'string' },
	    { arg: 'email', type: 'string' },
	    { arg: 'telefono', type: 'string' },
	    { arg: 'celular', type: 'string' },
	   	{ arg: 'fax', type: 'string' },
	    { arg: 'direccion', type: 'string' },
	    { arg: 'ciudad', type: 'string' },
	    { arg: 'formaDePago', type: 'string' },
	    { arg: 'numsol', type: 'number' },
	    { arg: 'contacto', type: 'string' },
	    { arg: 'cantidad1', type: 'number' },
	    { arg: 'cantidad2', type: 'number' },
	    { arg: 'cantidad3', type: 'number' },
	    { arg: 'cantidad4', type: 'number' },
	    { arg: 'cantidad5', type: 'number' },
	    { arg: 'cantidad6', type: 'number' }, 
	    { arg: 'descripcion', type: 'string' },
	    { arg: 'toleranciaNo', type: 'string' },
	    { arg: 'toleranciaSi', type: 'string' },
	    { arg: 'repeticionNo', type: 'string' },
	    { arg: 'repeticionSi', type: 'string' },
	    { arg: 'repOrdProd', type: 'string' },
	    { arg: 'alto', type: 'number' },
	    { arg: 'ancho', type: 'number' },
	    { arg: 'tipoPrecioFOB', type: 'string' },
	    { arg: 'tipoPrecioCIP', type: 'string' },
	    { arg: 'tipoPrecioOtro', type: 'string' },
	    { arg: 'sentidoEmbobinado', type: 'string' },
	    { arg: 'etiquetasRollo', type: 'string' },
	    { arg: 'puntasRectas', type: 'string' },
	    { arg: 'troquelNuevo', type: 'string' },
	    { arg: 'tintas', type: 'string' },
	    { arg: 'especiales', type: 'string' },
	    { arg: 'scratch', type: 'string' },
	    { arg: 'sustrato', type: 'string' },
	    { arg: 'adhesivo', type: 'string' },
	    { arg: 'brillo', type: 'string' },
	    { arg: 'laminacion', type: 'string' },
	    { arg: 'estampacion', type: 'string' },
	    { arg: 'repuje', type: 'string' },
	    { arg: 'informacionVari', type: 'string' },
	    { arg: 'codbar', type: 'string' },
	    { arg: 'numeracion', type: 'string' },
	    { arg: 'emb1', type: 'string' },
	    { arg: 'emb2', type: 'string' },
	    { arg: 'emb3', type: 'string' },
	    { arg: 'emb4', type: 'string' },
	    { arg: 'emb5', type: 'string' },
	    { arg: 'emb6', type: 'string' },
	    { arg: 'emb7', type: 'string' },
	    { arg: 'emb8', type: 'string' },
	    { arg: 'observaciones', type: 'string' }
	    
	  ],
	    returns: {
	      arg: 'info',
	      type: 'object',
	    },
	    http: {
	      path: '/crearReporteET',
	      verb: 'post',
	    },
	  });

	  function crearReporteET(dia, mes, anio, ejecutivo, empresa, nit, cargo, email, telefono, celular, fax, direccion, ciudad, formaDePago, numsol, contacto,
	  	cantidad1, cantidad2, cantidad3, cantidad4, cantidad5, cantidad6, descripcion, toleranciaNo, toleranciaSi, repeticionNo, repeticionSi,
		repOrdProd, alto, ancho, tipoPrecioFOB, tipoPrecioCIP, tipoPrecioOtro, sentidoEmbobinado, etiquetasRollo, puntasRectas, troquelNuevo, tintas, especiales, scratch,
		sustrato, adhesivo, brillo, laminacion, estampacion, repuje, informacionVari, codbar, numeracion, emb1, emb2, emb3, emb4, emb5, emb6, emb7, emb8, observaciones, cb) {
		client.render({
			    template: {
						    "name": "Etiquetas",
						    "recipe": "electron-pdf",
						    "engine": "handlebars"
						},
		    	data: { 
                                    "dia": dia, 
                                    "mes": mes,
                                    "anio": anio,
                                // Cliente
                                    "ejecutivo": ejecutivo, // vendedor: codVend
                                    "empresa": empresa , // ok pero ??
                                    "NIT": nit, // ok
                                    //"contacto": "FALTA", // de la solicitud
                                    "cargo": cargo, // ??
                                    "email": email, // ok

                                    "telefono": telefono, // ok
                                    "celular": celular, // ok
                                    "fax": fax, // ok 
     
                                    "direccion": direccion, // ok
                                    "ciudad": ciudad, // ok

                                    "formaDePago": formaDePago, // 
                                // Solicitud ETIQUETA
                                        "numsol" : numsol,
                                    // Contacto 
                                        "contacto": contacto,
                                    // Cantidades
                                        "cantidad1": cantidad1,
                                        "cantidad2": cantidad2,
                                        "cantidad3": cantidad3,
                                        "cantidad4": cantidad4,
                                        "cantidad5": cantidad5,
                                        "cantidad6": cantidad6,
                                    // Descripcion del trabajo 
                                        "descripcion": descripcion,
                                    // Tolerancia
                                        "toleranciaNo": toleranciaNo,
                                        "toleranciaSi": toleranciaSi,
                                        "repeticionNo": repeticionNo,
                                        "repeticionSi": repeticionSi,
                                    // Orden de produccion ??
                                        "repOrdProd": repOrdProd,
                                    // Tamaño
                                        "alto": alto,
                                        "ancho": ancho,
                                    // Tipo de Precio
                                        "tipoPrecioFOB": tipoPrecioFOB,
                                        "tipoPrecioCIP": tipoPrecioCIP,
                                        "tipoPrecioOtro": tipoPrecioOtro,
                                        
                                    // Acabados
                                        "sentidoEmbobinado": sentidoEmbobinado,
                                        "etiquetasRollo": etiquetasRollo,
                                        "puntasRectas": puntasRectas,
                                        "troquelNuevo": troquelNuevo,
                                        "tintas": tintas,
                                        "especiales": especiales,
                                        "scratch": scratch,
                                        "sustrato": sustrato,
                                        "adhesivo" : adhesivo,
                                        "brillo"   : brillo,
                                        "laminacion": laminacion,
                                        "estampacion": estampacion,
                                        "repuje": repuje,
                                        "informacionVari": informacionVari,
                                        "codbar"   : codbar,
                                        "numeracion": numeracion,
                                    // Embobinados 
                                        "emb1" : emb1, 
                                        "emb2" : emb2, 
                                        "emb3" : emb3, 
                                        "emb4" : emb4, 
                                        "emb5" : emb5, 
                                        "emb6" : emb6, 
                                        "emb7" : emb7, 
                                        "emb8" : emb8,
                                    // Observaciones
                                        "observaciones": observaciones
                                      
		    	}
		}).then(function(res){
			//console.log(res);
			fs.writeFileSync("Etiquetas.pdf", res.content);

		    res.body((buffer) => {
				console.log(buffer.toString());
			});
		});

	  };
};

	/*
	const request = require("request");

	Reports.ejecutarReport = ejecutarReport;

	  Reports.remoteMethod('ejecutarReport', {
		accepts: [
			{arg: 'dia', type: 'string'}
		],
	    returns: {
	      arg: 'info',
	      type: 'object',
	    },
	    http: {
	      path: '/crearReporteET',
	      verb: 'post',
	    },
	  });
	*/
	/*
	function ejecutarReport(dia, callback){
		console.log("Ejecutando ejecutarReport");

	    var options = { method: 'POST',
	      url: '//localhost:8081/jasperserver-pro/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Fpublic%2FSamples%2FReports&reportUnit=%2Fpublic%2FSamples%2FReports%2F08.UnitSalesDetailReport&standAlone=true',
	      headers: 
	       { 
	       	'cache-control': 'no-cache',
	         'content-type': 'application/json' },
	      body: 
	        { "name": "test",
	         "connection": "none",
	         "parameters": { "Parameter1": "Parameter 1", "Parameter2": "PARAM2'" } 
	     	},
	      json: true };

	    request(options, function (error, response, body) {
	      if (error) throw new Error(error);

	      console.log(body);
	    });
	}
	*/
	/*
	function ejecutarReport(){
		request.post({url: "http://localhost:8081/jasperserver-pro/rest/login", 
		        qs: {j_username: "jasperadmin", j_password: "jasperadmin"}},
		        function(err, res, body) {
		            if(err) {
		                return console.error(err);
		            }
		            else{
		            console.log("Ejecutar Report autentication OK");
		            request.get("http://localhost:8081/jasperserver-pro/rest_v2/reports/SampleQueryReport.pdf",
		                function (error, response, body1) {
		                    if (!error) {
		                          console.log("downloading")
		                    }
		                    else{
		                        console.log(response.statusCode);
		                        console.log(error);
		                    }
		            })
		        }
		    });
	}
	*/
