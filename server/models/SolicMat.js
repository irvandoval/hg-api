'use strict';

module.exports = function(SolicMat) {
	var app = require("../../server/server");
	var REMOTE_METHOD_DESC = 'Elimina todos los materiales en base al nsolic y al numitem';
    var REMOTE_METHOD_DESC2 = 'Modifica el material usando como pk nsolic y numitem';

	SolicMat.delByNSoliAndNumItem = delByNSoliAndNumItem;
    SolicMat.modByNSoliAndNumItem = modByNSoliAndNumItem;

	SolicMat.remoteMethod("delByNSoliAndNumItem", {
	    accepts: [
		    {
		      arg: 'nsolic',
		      type: 'number',
		    },

        {
          arg: 'consec',
          type: 'string',
        },
        {
          arg: 'nolinea',
          type: 'number',
        },
        {
          arg: 'noItem',
          type: 'number',
        },
	  	],
        description: REMOTE_METHOD_DESC,
        returns: { 
        	arg: "info", 
        	type: "object" 
        },
        http: { 
        	path: "/delByNSoliAndNumItem",
        	verb: "delete" 
        }
    });

    SolicMat.remoteMethod("modByNSoliAndNumItem", {
        accepts: [
            {
              arg: 'nsolic',
              type: 'number',
            },
            {
              arg: 'noItem',
              type: 'number',
            },
            {
              arg: 'objMaterial',
              type: 'object',
            },
        ],
        description: REMOTE_METHOD_DESC,
        returns: { 
            arg: "info", 
            type: "object" 
        },
        http: { 
            path: "/modByNSolicAndCodMat",
            verb: "post" 
        }
    });

  function delByNSoliAndNumItem(nsolic, consec, nolinea, noItem, cb) {
    console.log(nsolic + ' ' + noItem);
    if (nsolic  && noItem){
    	SolicMat.destroyAll({nsolic: nsolic, consec: consec, nolinea: nolinea, noItem: noItem})
        .then(function(info) {
          cb(null, info);
        })
        .catch(function(err) {
          cb(err, null);
        });
    }else{ 
    	cb(new Error('Not Found arguments'), null);
    }
  };

  function modByNSoliAndNumItem(nsolic, noItem, objMaterial, cb) {
    //console.log(nsolic + ' ' + noItem);
    if (nsolic  && noItem){
        SolicMat.updateAll({nsolic: nsolic, noItem: noItem}, objMaterial)
        .then(function(info) {
          cb(null, info);
        })
        .catch(function(err) {
          cb(err, null);
        });
    }else{ 
        cb(new Error('Not Found arguments'), null);
    }
  };

}