var express = require('express'),
    app = express(),
    jasper = require('node-jasper')({
        path: 'lib/jasperreports-6.4.3.jar',
        reports: {
            hw: {
                jasper: 'reports/helloWorld.jasper'
            }
        },
        drivers: {
            sqlser: {
                path: 'lib/sqljdbc4-3.0.jar',
                class: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
                type: 'sqlserver'
            },
            pg: {
                path: 'lib/postgresql-42.1.4.jar',
                class: 'org.posgresql.Driver',
                type: 'postgresql'
            }

        },
        conns: {
            accountDB: {
                name: "accountDB",
                connector: "mssql",
                url: "mssql://sa:admin21@localhost:1433/MSSQLSERVER/Cotizacion_Hybrid?connectionTimeout=25000&requestTimeout=25000"
            },
            accountPG: {
                host: "localhost",
                port: 5432,
                url: "postgres://postgres:postgres@localhost/Hogier",
                database: "Hogier",
                password: "postgres",
                name: "accountPG",
                user: "postgres",
                connector: "postgresql"
            }
        },
        defaultConn: 'accountDB'
    });

app.get('/pdf', function(req, res, next) {
    //beware of the datatype of your parameter.
    var report = { report: 'hw', data: { id: parseInt(req.query.id, 10) } };
    var pdf = jasper.pdf(report);
    res.set({
        'Content-type': 'application/pdf',
        'Content-Length': pdf.length
    });
    res.send(pdf);
});

app.listen(3000);